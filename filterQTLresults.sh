#!/bin/bash

directory=$1

inputFile1="top_qtl_results_all.txt"
inputFile1="$directory$inputFile1"

inputFile2="qtl_results_all.txt"
inputFile2="$directory$inputFile2"

outputFile="final_qtl_results_all.txt"
outputFile="$directory$outputFile"

maxPValue=$(Rscript Rscripts/get_qValues.R $inputFile1 0.05)

awk -v myvar="$maxPValue" '{ if ($3 < myvar) {print}}' $inputFile2 > $outputFile 
