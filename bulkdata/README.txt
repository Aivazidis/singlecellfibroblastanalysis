output1a: Output from running all bulk samples through the eQTL pipeline the first time
output1b: Output from running all bulk samples through the eQTL pipeline a second time
output_0h: Output from running all bulk samples in the unstimulated condition through the eQTL pipeline
output_2h: Output from running all bulk samples in the 2h stimulated condition through the eQTL pipeline
output_6h: Output from running all bulk samples in the 6h stimulated condition through the eQTL pipeline
