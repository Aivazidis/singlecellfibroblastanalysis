"""
Test Snakefile for  genotyping
Author: Marc Jan Bonder
Affiliation: EMBL-EBI
Date: Tuesday 10 February 2018
#Run: snakemake --snakefile snakemake_eQTL_pipeline_0hBulk.py --jobs 250 --latency-wait 30 --cluster-config cluster.json --cluster 'bsub -q {cluster.queue} -n {cluster.n} -R "rusage[mem={cluster.memory}]" -M {cluster.memory} -o ./cis_eqlts.20180426.o -e ./cis_eqlts.20180426.e'
"""

import glob
import os
from subprocess import run
import pandas as pd
import re
from os.path import join

shell.prefix("set -euo pipefail;")

def _multi_arg_start(flag, files):
    flag += " "
    return " ".join(flag + f for f in files)

def _multi_arg_end(flag, files):
    flag = " "+flag
    return " ".join(f + flag for f in files)

def _multi_arg_both_ends(flag1, flag2, files):
    flag1 += " "
    flag2 = " "+flag2
    return " ".join(flag1 + f + flag2 for f in files)

def flatenChunk(chunk):
    return chunk.replace(":", "_").replace("-", "_")

def extendChunk(chunk):
    relChunk = chunk.pop()
    chunkSplitted = relChunk.split("_")
    return chunkSplitted[0]+":"+chunkSplitted[1]+"-"+chunkSplitted[2]

#Variables
chunkFile = '/hps/nobackup/hipsci/scratch/trans_eqtls/QTL_Mapping/splitFiles/Ensembl_75_Limix_Annotation_FC_Gene_step200.txt'
genotypeFile =  '/hps/nobackup/hipsci/scratch/genotypes/imputed/REL-2018-01/Full_Plink/hipsci.wec.gtarray.HumanCoreExome.imputed_phased.20180102.genotypes.norm.renamed.recode.vcf.gz'
annotationFile = '/hps/nobackup/hipsci/scratch/trans_eqtls/QTL_Mapping/Ensembl_75_Limix_Annotation_FC_Gene.txt'
phenotypeFile =  '/hps/nobackup/hipsci/scratch/singlecell_fibroblast/alexanderAnalysis/singlecellfibroblastanalysis/singleCell/meanMatrix.txt' #/hps/nobackup/stegle/users/rrostom/limix_020818/genes_donors_IFN_2h_mean_matrix.txt'
covariateFile = '/hps/nobackup/hipsci/scratch/singlecell_fibroblast/alexanderAnalysis/singlecellfibroblastanalysis/singleCell/covariatesMatrix_batch0_condpIC_6h.txt'
kinshipFile =  '/hps/nobackup/hipsci/scratch/genotypes/imputed/REL-2018-01/Full_Filtered_Plink-f/hipsci.wec.gtarray.HumanCoreExome.imputed_phased.20170327.genotypes.norm.renamed.recode.filtered.rel'
sampleMappingFile = '/hps/nobackup/hipsci/scratch/singlecell_fibroblast/alexanderAnalysis/singlecellfibroblastanalysis/singleCell/sampleMapping.txt'#/hps/nobackup/stegle/users/rrostom/limix_020818/sample_mapping_file_individuals.txt'
#variantFilterFile = '/hps/nobackup/hipsci/scratch/trans_eqtls/QTL_Mapping/GWAS_e92_r2018_04_10_GR37_i2_Ids.txt'
#extendedAnnotationFile = '/hps/nobackup/hipsci/scratch/trans_eqtls/QTL_Mapping/Crosmapping_file_processed.txt'
#featureFilterFile = '/hps/nobackup/hipsci/scratch/trans_eqtls/QTL_Mapping/Ensembl_75_proteinCodingGenes_expressed_25p_samples.txt'
numberOfPermutations = '1000'
numberOfPermutations_wp = '10'
minorAlleleFrequency = '0.05'
hwe = '0.0000001'
callRate = '0.95'
windowSize = '100000'
blockSize = '1000'
outputFolder = '/hps/nobackup/hipsci/scratch/singlecell_fibroblast/alexanderAnalysis/singlecellfibroblastanalysis/singleCell/output_6h_batch1/' #'/hps/nobackup/stegle/users/rrostom/limix_020818/means/output_IFN_2h/'
finalQtlRun =  '/hps/nobackup/hipsci/scratch/singlecell_fibroblast/alexanderAnalysis/singlecellfibroblastanalysis/singleCell/output_6h_batch1/top_qtl_results_all.txt'
finalQtlRun2 =  '/hps/nobackup/hipsci/scratch/singlecell_fibroblast/alexanderAnalysis/singlecellfibroblastanalysis/singleCell/output_6h_batch1/qtl_results_all.txt'

with open(chunkFile,'r') as f:
    chunks = [x.strip() for x in f.readlines()]

qtlOutput = []
for chunk in chunks:
    #print(chunk)
    processedChunk = flatenChunk(chunk)
    #print(processedChunk)
    processedChunk=expand(outputFolder+'{chunk}.finished',chunk=processedChunk )
    qtlOutput.append(processedChunk)

## flatten these lists
qtlOutput = [filename for elem in qtlOutput for filename in elem]
#finalQtlRun = [filename for elem in finalQtlRun for filename in elem]

rule all:
    input:
        qtlOutput, finalQtlRun, finalQtlRun2

rule run_qtl_mapping:
    input:
        af = annotationFile,
        pf = phenotypeFile,
        cf = covariateFile,
        kf = kinshipFile,
        #ff = featureFilterFile,
        #vf = variantFilterFile,
        #eaf = extendedAnnotationFile,
        smf = sampleMappingFile
    output:
        outputFolder + '{chunk}.finished'
    params:
        gen=genotypeFile,
        od = outputFolder,
        np = numberOfPermutations_wp,
        maf = minorAlleleFrequency,
        hwe = hwe,
        cr = callRate,
        w = windowSize,
        bs = blockSize
    run:
        chunkFull = extendChunk({wildcards.chunk})
        shell(
            "/nfs/software/stegle/users/mjbonder/conda-envs/limix_qtl/bin/python /hps/nobackup/stegle/users/mjbonder/tools/hipsci_pipeline/limix_QTL_pipeline/run_QTL_analysis.py "
            "--plink {params.gen} "
            " -af {input.af} "
            " -pf {input.pf} "
            " -cf {input.cf} "
            " -od {params.od} "
            " --kinship_file {input.kf} "
            " --sample_mapping_file {input.smf} "
            " -gr {chunkFull} "
            " -np {params.np} "
            " -maf {params.maf} "
            " -hwe {params.hwe} "
            " -cr {params.cr} "
            " -c -gm standardize "
            " -w {params.w} "
            #" --variant_filter {input.vf} "
            #" --feature_filter {input.ff} "
            #" --extended_annotation_file {input.eaf} "
            " --block_size {params.bs} ")
        shell("touch {output}")

rule run_qtl_mapping_wp:
    input:
        af = annotationFile,
        pf = phenotypeFile,
        cf = covariateFile,
        kf = kinshipFile,
        #ff = featureFilterFile,
        #vf = variantFilterFile,
        #eaf = extendedAnnotationFile,
        smf = sampleMappingFile
    output:
        outputFolder + 'wp/{chunk}.finished'
    params:
        gen=genotypeFile,
        od = outputFolder,
        np = numberOfPermutations,
        maf = minorAlleleFrequency,
        hwe = hwe,
        cr = callRate,
        w = windowSize,
        bs = blockSize
    run:
        chunkFull = extendChunk({wildcards.chunk})
        shell(
            "/nfs/software/stegle/users/mjbonder/conda-envs/limix_qtl/bin/python /hps/nobackup/stegle/users/mjbonder/tools/hipsci_pipeline/limix_QTL_pipeline/run_QTL_analysis.py "
            "--plink {params.gen} "
            " -af {input.af} "
            " -pf {input.pf} "
            " -cf {input.cf} "
            " -od {params.od} "
            " --kinship_file {input.kf} "
            " --sample_mapping_file {input.smf} "
            " -gr {chunkFull} "
            " -np {params.np} "
            " -wp "
            " -maf {params.maf} "
            " -hwe {params.hwe} "
            " -cr {params.cr} "
            " -c -gm standardize "
            " -w {params.w} "
            #" --variant_filter {input.vf} "
            #" --feature_filter {input.ff} "
            #" --extended_annotation_file {input.eaf} "
            " --block_size {params.bs} ")
        shell("touch {output}")

rule aggregate_qtl_results:
    input:
        IF = outputFolder,
        OF = outputFolder,
        finalFiles = qtlOutput
    output:
        outputFolder + 'top_qtl_results_all.txt'
    run:
        shell(
            "/nfs/software/stegle/users/mjbonder/conda-envs/limix_qtl/bin/python /hps/nobackup/stegle/users/mjbonder/tools/hipsci_pipeline/post-processing_QTL/minimal_postprocess.py "
            "-id {input.IF} "
            " -od {input.OF} "
            " -sfo -tfb ")

rule aggregate_all_qtl_results:
    input:
        IF = outputFolder,
        OF = outputFolder,
        finalFiles = qtlOutput
    output:
        outputFolder + 'qtl_results_all.txt'
    run:
        shell(
            "/nfs/software/stegle/users/mjbonder/conda-envs/limix_qtl/bin/python /hps/nobackup/stegle/users/mjbonder/tools/hipsci_pipeline/post-processing_QTL/minimal_postprocess.py "
            "-id {input.IF} "
            " -od {input.OF} "
            " -sfo ")
