#!/bin/bash

directory=$1

inputFile1="top_qtl_results_all.txt"
inputFile1="$directory$inputFile1"



outputFile1="top_FDR0.1_qtl_results_all.txt"
outputFile1="$directory$outputFile1"

outputFile2="top_FDR0.05_qtl_results_all.txt"
outputFile2="$directory$outputFile2"

outputFile3="top_FDR0.01_qtl_results_all.txt"
outputFile3="$directory$outputFile3"

outputFile4="top_FDR0.001_qtl_results_all.txt"
outputFile4="$directory$outputFile4"


maxPValue1=$(Rscript Rscripts/get_qValues.R $inputFile1 0.1)
awk -v myvar="$maxPValue1" '{ if ($3 < myvar) {print}}' $inputFile1 > $outputFile1

maxPValue2=$(Rscript Rscripts/get_qValues.R $inputFile1 0.05)
awk -v myvar="$maxPValue2" '{ if ($3 < myvar) {print}}' $inputFile1 > $outputFile2

maxPValue3=$(Rscript Rscripts/get_qValues.R $inputFile1 0.01)
awk -v myvar="$maxPValue3" '{ if ($3 < myvar) {print}}' $inputFile1 > $outputFile3

maxPvalue4=$(Rscript Rscripts/get_qValues.R $inputFile1 0.001)
awk -v myvar="$maxPValue4" '{ if ($3 < myvar) {print}}' $inputFile1 > $outputFile4 
