#!/bin/bash

directory=$1

inputFile1="qtl_results_all.txt"
inputFile1="$directory$inputFile1"

outputFile="nominal_qtl_results_all.txt"
outputFile="$directory$outputFile"

maxPValue=0.05

awk -v myvar="$maxPValue" '{ if ($3 < myvar) {print}}' $inputFile1 > $outputFile 
