# Get mean matrix
# Prepare mean matrices to give to the eQTL pipeline

require(peer)
data = read.delim('singleCell/counts_merged_donors_cardelino_donorid_all_qc_filt.txt.gz', fill = T, comment.char = "")
rownames(data) = data[,1]
data = data[,2:dim(data)[2]]
sampleNames = colnames(data)
sampleNames = gsub('\\.', '#', sampleNames)
sampleNames = sub('.','', sampleNames)
colnames(data) = sampleNames
#data = log(data + 1)

coldata = read.delim('singleCell/coldata_merged_donors_cardelino_donorid_all_qc_filt.txt.gz')
coldata = coldata[coldata[,'sample_id'] %in% colnames(data),]
coldata = coldata[match(colnames(data), coldata[,'sample_id']),]

# Now we make a mean matrix in which data from the same donor with the same experiment id
# was averaged
# We also count the average number of detected genes to use it as a covariate

donors = coldata[,'donor_short_id']
ids = coldata[,'experiment_id']
conditions = coldata[,'well_condition']
total_counts = coldata[,'total_counts']

combined = paste(donors,ids, sep = "+")
combined = paste(combined, conditions, sep = "+")

meanMatrix = matrix(0, dim(data)[1], length(unique(combined)))
colnames(meanMatrix) = unique(combined)
rownames(meanMatrix) = rownames(data)
cdr = colSums(data > 0)
meanCDR = rep(0, length(unique(combined)))
names(meanCDR) = unique(combined)
coldata = matrix('0', length(unique(combined)), 3)
colnames(coldata) = c('donor', 'experiment_id', 'well_condition')
raghdMatrix = read.delim('../../raghd_analysis/limix/limix_020818/covariate_subset.txt', fill = T, comment.char = "")
# This is raghd's covariate matrix
gender = raghdMatrix[,1]
names(gender) = rownames(raghdMatrix)
age = raghdMatrix[,2]
names(age) = rownames(raghdMatrix)
covariateMatrix = matrix(0,length(unique(combined)), 2)
colnames(covariateMatrix) = c('gender', 'age')
rownames(covariateMatrix) = colnames(meanMatrix)
subsetSize = rep(0,dim(meanMatrix)[2])

for (i in 1:dim(meanMatrix)[2])
{
  print(i)
  donor = strsplit(colnames(meanMatrix)[i], split = "\\+")[[1]][1]
  id = strsplit(colnames(meanMatrix)[i], split = "\\+")[[1]][2]
  condition = strsplit(colnames(meanMatrix)[i], split = "\\+")[[1]][3]
  subset = which(donors == donor & ids == id & conditions == condition)
  subsetSize[i] = length(subset)
  meanMatrix[,i] = apply(data[,subset],1,function(x) mean(x))
  meanCDR[i] = mean(cdr[subset])
  coldata[i,1] = donor
  coldata[i,2] = id
  coldata[i,3] = condition
  covariateMatrix[i,'age'] = age[donor]
  covariateMatrix[i,'gender'] = gender[donor]
}

genes = rownames(meanMatrix)
samples = colnames(meanMatrix)
samples = c('genes', samples)
meanMatrixNew = cbind(genes,meanMatrix)
meanMatrixNew = rbind(samples,meanMatrixNew)
meanMatrixNew = unname(meanMatrixNew)

# Need to change gene names!
ensemble = unlist(lapply(as.character(meanMatrixNew[2:dim(meanMatrix)[1],1]), function(x) strsplit(x, split = '_')[[1]][[1]]))
symbol = unlist(lapply(as.character(meanMatrixNew[2:dim(meanMatrix)[1],1]), function(x) strsplit(x, split = '_')[[1]][[2]]))
geneMapping = cbind(ensemble, symbol)
write.table(geneMapping, file = 'singleCell/geneMapping.txt', sep = '\t', quote = F, col.names = F, row.names = F)
meanMatrixNew[2:dim(meanMatrix)[1],1] = ensemble
#colnames(meanMatrix) = gsub("\\.", "\\+", colnames(meanMatrix))
#write.table(meanMatrixNew, file = 'singleCell/meanMatrix.txt', sep = "\t", quote = F, col.names = T, row.names = F)

write.table(meanMatrixNew, file = 'singleCell/meanMatrix.txt', sep = "\t", quote = F, col.names = F, row.names = F)
write.table(meanCDR, file = 'singleCell/meanCDR.txt', quote = F, col.names = T, row.names = T)
write.table(coldata, file = 'singleCell/coldataMeanMatrix.txt', quote = F, col.names = T, row.names = T)
write.table(subsetSize, file = 'singleCell/subsetSize.txt', quote = F, col.names = F, row.names = F)

# Next we get Peer hidden factors for each of the 5 conditions and save them as covariate files:

number_hiddenFactors = 8
for (i in 1:length(unique(conditions)))
{
  cond = unique(conditions)[i]
  model = PEER()
  PEER_setNk(model, number_hiddenFactors)
  PEER_setPhenoMean(model, as.matrix(t(meanMatrix[,coldata[,3] == cond])))
  PEER_setCovariates(model, as.matrix(covariateMatrix[coldata[,3] == cond,]))
  PEER_setNmax_iterations(model,1000)
  PEER_update(model)
  factors = PEER_getX(model)
  colnames(factors) = c('gender', 'age',
                        paste(rep('hiddenFactor', number_hiddenFactors), as.character(1:number_hiddenFactors), sep = ''))
  rownames(factors) = colnames(meanMatrix)[coldata[,3] == cond]
  write.table(factors, paste('singleCell/covariatesMeanMatrix1_', cond, 'HF', number_hiddenFactors,
                             '.txt', sep = ""), sep = "\t", quote = F, row.names = T, col.names = T)
}

# Finally we get a sample mapping file:

bulkMapping = read.delim('/hps/nobackup/hipsci/scratch/singlecell_fibroblast/alexanderAnalysis/singlecellfibroblastanalysis/sampleMappingGRCh37.txt', fill = T, comment.char = "")
phenoNames = rownames(covariateMatrix)
genoNames = unique(as.character(bulkMapping[,1]))
genoDonors = unlist(lapply(genoNames, function(x) strsplit(x, split = '-')[[1]][[2]]))
names(genoNames) = genoDonors
phenoDonors = unlist(lapply(phenoNames, function(x) strsplit(x, split = '//+')[[1]][[1]]))
genoNamesMapping = genoNames[phenoDonors]
sampleMapping = cbind(genoNamesMapping, phenoNames)
sampleMapping = sampleMapping[!is.na(sampleMapping[,1]),]
colnames(sampleMapping) = colnames(bulkMapping)
write.table(sampleMapping, file = 'singleCell/sampleMapping.txt', sep = '\t', quote = F, col.names = T, row.names = F)

addCornerValue = function(m,value)
{
  r = as.character(rownames(m))
  s = as.character(colnames(m))
  s = c(value, s)
  m = cbind(r,m)
  m = rbind(s,m)
  m = unname(m)
  return(m)
}

covariateMatrix = addCornerValue(covariateMatrix, 'samples')

write.table(covariateMatrix, file = 'singleCell/covariateMatrixMeanMatrix.txt',
            sep = "\t", quote = F, col.names = F, row.names = F)

for (i in 1:length(unique(conditions)))
{
  cond = unique(conditions)[i]
  factors = read.delim(paste('singleCell/covariatesMeanMatrix_', cond, '.txt', sep = ""), sep = "", fill = T, comment.char = "")
  write.table(factors, paste('singleCell/covariatesMeanMatrix_', cond, '.txt', sep = ""), sep = "\t", quote = F, row.names = T, col.names = )
}



